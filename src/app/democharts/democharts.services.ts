import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';


// const API_ENDPOINT = 'https://jsonplaceholder.typicode.com/posts';
const API_ENDPOINT = 'http://localhost:7000';

/**
 * A simple service which fetches data from HTTP API.
 *
 * TODO: Move API endpoint to app config
 */
@Injectable()
export class DemoChartServices {
    constructor(private http: Http) {

    }

    public getAll() {
        return this.http
            .get(API_ENDPOINT+'/listTestData')
            .map(this.extractData);
    }

    private extractData(res: Response) {
        let body = res.json();
        return body || { };
    }

}
