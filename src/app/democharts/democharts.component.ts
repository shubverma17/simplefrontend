import { Component, OnInit } from '@angular/core';
import { DemoChartServices } from './democharts.services';
import { Response } from '@angular/http';

@Component({
  selector: 'app-democharts',
  templateUrl: './democharts.component.html',
  styleUrls: ['./democharts.component.css'],
  providers: [DemoChartServices]
})
export class DemochartsComponent implements OnInit {
	private error: Response;
	private isLoading: boolean = true;
	options;
    data;
  constructor(
  	private demochartservice: DemoChartServices
  	) { }

  ngOnInit() {

  	this.options = {
      chart: {
        type: 'discreteBarChart',
        height: 450,
        margin : {
          top: 20,
          right: 20,
          bottom: 50,
          left: 55
        },
        x: function(d){return d.label;},
        y: function(d){return d.value;},
        showValues: true,
        valueFormat: function(d){
          return d3.format(',.4f')(d);
        },
        duration: 500,
        xAxis: {
          axisLabel: 'X Axis'
        },
        yAxis: {
          axisLabel: 'Y Axis',
          axisLabelDistance: -10
        }
      }
    }
    this.demochartservice.getAll().subscribe(
        data  => {console.log(data);
            this.data = [
            {
                key:"Cumulative Return",
                values: data.chartvalues
            }
            ]},
        error => this.error = error,
        ()      => this.isLoading = false

    );
  }

}
