/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { DemochartsComponent } from './democharts.component';

describe('DemochartsComponent', () => {
  let component: DemochartsComponent;
  let fixture: ComponentFixture<DemochartsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DemochartsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DemochartsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
