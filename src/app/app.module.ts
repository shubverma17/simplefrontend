import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { NvD3Component } from 'ng2-nvd3';

import 'd3';
import 'nvd3';
import { DemochartsComponent } from './democharts/democharts.component';

@NgModule({
  declarations: [
    AppComponent,
    NvD3Component,
    DemochartsComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
