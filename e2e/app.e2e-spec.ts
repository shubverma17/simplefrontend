import { TestfrontendPage } from './app.po';

describe('testfrontend App', function() {
  let page: TestfrontendPage;

  beforeEach(() => {
    page = new TestfrontendPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
